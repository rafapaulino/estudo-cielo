<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Httpful' => array($vendorDir . '/nategood/httpful/src'),
    'Cielo' => array($vendorDir . '/developercielo/webservice-1.5-php/src'),
);
